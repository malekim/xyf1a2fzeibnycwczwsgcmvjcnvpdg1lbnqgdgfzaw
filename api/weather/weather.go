package weather

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
)

// IWeather interface holds all implemented methods.
type IWeather interface {
	SetApiUrl(apiUrl string) error
	SetApiKey(apiKey string)
	GetByCity(city string) (WeatherData, error)
}

// WeatherAPI struct is main struct to communicate with
// openweathermap API.
type WeatherAPI struct {
	apiUrl string
	apiKey string
}

// NewWeatherAPI returns instance of WeatherAPI.
func NewWeatherAPI() *WeatherAPI {
	return &WeatherAPI{}
}

// SetApiUrl is used to set apiUrl param.
func (w *WeatherAPI) SetApiUrl(apiUrl string) error {
	// just check if apiUrl is fine
	_, err := url.Parse(apiUrl)
	if err != nil {
		return err
	}
	w.apiUrl = apiUrl
	return nil
}

// SetApiKey is used to set apiKey param.
func (w *WeatherAPI) SetApiKey(apiKey string) {
	w.apiKey = apiKey
}

// GetyByCity func is used to read city weather from
// openweathermap API.
func (w *WeatherAPI) GetByCity(city string) (WeatherData, error) {
	wd := WeatherData{}
	requestUrl := fmt.Sprintf("%s/weather?q=%s&appid=%s", w.apiUrl, city, w.apiKey)
	response, err := http.Get(requestUrl)
	if err != nil {
		return wd, err
	}

	defer response.Body.Close()

	if response.StatusCode != http.StatusOK {
		return wd, fmt.Errorf("Status %d. Possible city %s does not exists or you provided wrong API key.", response.StatusCode, city)
	}

	if err := json.NewDecoder(response.Body).Decode(&wd); err != nil {
		return wd, err
	}

	return wd, nil
}
