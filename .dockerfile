FROM golang:1.13

RUN apt-get update && \
    apt-get install -qq \ 
    git \
    curl \
    xvfb \
    gdebi \
    openssl \
    git-core \
    libx11-dev \
    libssl-dev \
    xfonts-base \
    libfontenc1 \
    libjpeg-dev \
    libxext-dev \
    xfonts-75dpi \
    xfonts-utils \
    fontconfig -y \
    libfontconfig \
    libxrender-dev \
    build-essential \
    libfreetype6-dev \
    xfonts-encodings \
    libfontconfig1-dev
    
RUN go get github.com/cosmtrek/air

EXPOSE 8090

ENTRYPOINT ["/go/bin/air"]