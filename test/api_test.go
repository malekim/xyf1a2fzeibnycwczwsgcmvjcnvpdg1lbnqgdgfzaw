package test

import (
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/api/weather"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app/models"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app/services"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/go-redis/redis/v8"
	"github.com/joho/godotenv"
)

func setupTestCase(t *testing.T) (*app.Core, *weather.WeatherAPI) {
	err := godotenv.Load("../.env")
	if err != nil {
		t.Fatal(err)
	}

	app := &app.Core{}

	app.Configure()

	weatherAPI := weather.NewWeatherAPI()
	err = weatherAPI.SetApiUrl(os.Getenv("API_URL"))
	if err != nil {
		t.Fatal("Wrong API url")
	}

	weatherAPI.SetApiKey(os.Getenv("API_KEY"))

	mr, _ := miniredis.Run()
	r := redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})
	app.Redis = r
	return app, weatherAPI
}

func TestMain(t *testing.T) {
	app, weatherAPI := setupTestCase(t)

	wctrl := &services.Service{
		Model: &models.Model{
			Redis:      app.Redis,
			WeatherAPI: weatherAPI,
		},
	}

	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(wctrl.Index)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func TestWeatherGet(t *testing.T) {
	app, weatherAPI := setupTestCase(t)

	wctrl := &services.Service{
		Model: &models.Model{
			Redis:      app.Redis,
			WeatherAPI: weatherAPI,
		},
	}

	req, err := http.NewRequest("GET", "/weather?city=Olsztyn&city=Gdansk", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(wctrl.GetWeather)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("%v", rr)
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}
}

func TestWeatherGetEmpty(t *testing.T) {
	app, weatherAPI := setupTestCase(t)

	wctrl := &services.Service{
		Model: &models.Model{
			Redis:      app.Redis,
			WeatherAPI: weatherAPI,
		},
	}

	req, err := http.NewRequest("GET", "/weather", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(wctrl.GetWeather)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("Handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}
}
