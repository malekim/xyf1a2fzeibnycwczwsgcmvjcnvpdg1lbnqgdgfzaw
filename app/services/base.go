package services

import (
	"encoding/json"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app/models"
	"net/http"
)

type Service struct {
	Model *models.Model
}

func (s *Service) Index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(struct {
		Message string `json:"message"`
	}{
		"Message",
	})
}
