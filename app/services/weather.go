package services

import (
	"encoding/json"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/api/weather"
	"net/http"
)

func (s *Service) GetWeather(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	cities, _ := r.URL.Query()["city"]
	if len(cities) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{
			"Please provide at least one city",
		})
		return
	}

	ctx := r.Context()
	results, err := s.Model.GetWeather(ctx, cities)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		json.NewEncoder(w).Encode(struct {
			Message string `json:"message"`
		}{
			err.Error(),
		})
		return
	}
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(struct {
		Weather map[string]weather.WeatherData `json:"weather"`
	}{
		results,
	})
}
