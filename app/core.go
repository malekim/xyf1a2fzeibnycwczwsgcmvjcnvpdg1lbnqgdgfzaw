package app

import (
	"fmt"
	"log"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/api/weather"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app/models"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/app/services"
	"net/http"
	"os"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/go-redis/redis/v8"
)

type Core struct {
	Redis  *redis.Client
	Router chi.Router
}

func (c *Core) Configure() {
	c.connectRedis()
	c.setRoutes()
}

func (c *Core) connectRedis() {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", os.Getenv("REDIS_HOST"), os.Getenv("REDIS_PORT")),
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	c.Redis = client
}

func (c *Core) setRoutes() {
	r := chi.NewRouter()

	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.Timeout(60 * time.Second))
	r.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Access-Control-Allow-Origin", "Authorization", "Content-Type", "X-CSRF-Token"},
		ExposedHeaders:   []string{"Link"},
		AllowCredentials: true,
		// Maximum value not ignored by any of major browsers
		MaxAge: 300,
	}))

	weatherAPI := weather.NewWeatherAPI()
	err := weatherAPI.SetApiUrl(os.Getenv("API_URL"))
	if err != nil {
		log.Fatalf("Wrong API url")
	}

	weatherAPI.SetApiKey(os.Getenv("API_KEY"))

	wctrl := &services.Service{
		Model: &models.Model{
			Redis:      c.Redis,
			WeatherAPI: weatherAPI,
		},
	}

	r.Get("/", wctrl.Index)
	r.Get("/weather", wctrl.GetWeather)

	c.Router = r
}

func (c *Core) Start() {
	port := fmt.Sprintf(":%s", os.Getenv("HTTP_PORT"))
	fmt.Println(fmt.Sprintf("Server started on %s", port))
	http.ListenAndServe(port, c.Router)
}
