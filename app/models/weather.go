package models

import (
	"context"
	"encoding/json"
	"fmt"
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/api/weather"
	"time"

	"github.com/go-redis/redis/v8"
)

// GetWeather func is collecting weather data
// for each requested city.
func (m *Model) GetWeather(ctx context.Context, cities []string) (map[string]weather.WeatherData, error) {
	// prevent error if empty
	results := make(map[string]weather.WeatherData, 0)

	for _, city := range cities {
		weather, err := m.GetWeatherByCity(ctx, city)
		if err != nil {
			return results, err
		}
		results[city] = weather
	}
	return results, nil
}

// GetWeatherByCity tries to read weather from cache.
// If it fails, then it tries to read weather from API
// and save it to cache. Cache duration is set for 60 minutes.
func (m *Model) GetWeatherByCity(ctx context.Context, city string) (weather.WeatherData, error) {
	var data weather.WeatherData = weather.WeatherData{}
	cacheKey := fmt.Sprintf("city-%s", city)
	cacheVal, err := m.Redis.Get(ctx, cacheKey).Result()
	if err == redis.Nil {
		result, apiErr := m.WeatherAPI.GetByCity(city)
		if apiErr != nil {
			return data, apiErr
		}
		if cacheErr := m.CacheResult(ctx, cacheKey, result); cacheErr != nil {
			return data, cacheErr
		}
		return result, nil
	}
	if err != nil {
		return data, err
	}
	jsonErr := json.Unmarshal([]byte(cacheVal), &data)
	if jsonErr != nil {
		return data, jsonErr
	}
	return data, nil
}

// CacheResult cache result of GetWeatherByCity func
func (m *Model) CacheResult(ctx context.Context, cacheKey string, result weather.WeatherData) error {
	data, err := json.Marshal(result)
	if err != nil {
		return err
	}
	// cache is valid hor one hour
	cacheExpiration := 60 * time.Minute
	err = m.Redis.Set(ctx, cacheKey, data, cacheExpiration).Err()
	if err != nil {
		return err
	}
	return nil
}
