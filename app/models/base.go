package models

import (
	"malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw/api/weather"

	"github.com/go-redis/redis/v8"
)

type Model struct {
	Redis      redis.Cmdable
	WeatherAPI weather.IWeather
}
