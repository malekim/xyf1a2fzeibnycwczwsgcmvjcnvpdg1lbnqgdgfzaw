# Microservice for the openweathermap API

Microservice allows to read weather data from few cities.

## How to run

You can generate an api key on: https://openweathermap.org/

Copy .env.example to .env and set all variables. Do not forget to set API_KEY. 

```bash
cp .env.example .env
```

Then run docker-compose:

```bash
docker-compose up
```

## How to use

To read data from few cities, just pass them as get parameter "city".

```bash
${API_URL}/weather?city=London&city=Olsztyn&city=Gdansk&city=Berlin
```

## Tests

Run tests with that command:

```bash
go test ./test
```