module malekim/xYF1a2FzeiBNYcWCZWsgcmVjcnVpdG1lbnQgdGFzaw

go 1.13

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-redis/redis/v8 v8.0.0-beta.4
	github.com/gomodule/redigo v1.8.2 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/yuin/gopher-lua v0.0.0-20200603152657-dc2b0ca8b37e // indirect
)
